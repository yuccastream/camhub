package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCamera_SetXAddr(t *testing.T) {
	c := &Camera{}
	c.SetXAddr("http://192.168.84.201/onvif/device_service")
	assert.Equal(t, "http://192.168.84.201/onvif/device_service", c.XAddr)
	assert.Equal(t, "192.168.84.201", c.IP)

	c.SetXAddr("http://192.168.84.203:10080/onvif/device_service")
	assert.Equal(t, "http://192.168.84.203:10080/onvif/device_service", c.XAddr)
	assert.Equal(t, "192.168.84.203", c.IP)
}
