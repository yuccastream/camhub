package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (h *Hub) setV1Routes(router *mux.Router) {
	routes := router.PathPrefix("/v1").Subrouter()
	hub := routes.PathPrefix("/hub").Subrouter()
	hub.HandleFunc("/status", h.GetHubStatus).Methods(http.MethodGet)
	hub.HandleFunc("/on", h.SetHubOn).Methods(http.MethodPost)
	hub.HandleFunc("/off", h.SetHubOff).Methods(http.MethodPost)

	cam := routes.PathPrefix("/cameras").Subrouter()
	cam.HandleFunc("", h.GetCameras).Methods(http.MethodGet)
	cam.HandleFunc("/{id}", h.GetCamera).Methods(http.MethodGet)
}
