package yandex

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

const baseURL = "https://api.iot.yandex.net/v1.0"

type Client struct {
	httpCli *http.Client
	token   string
}

func NewClient(token string) *Client {
	client := http.DefaultClient
	return &Client{
		httpCli: client,
		token:   token,
	}
}

func (c *Client) newRequest(method, urlStr string, body interface{}) (*http.Request, error) {
	u := joinURLPath(baseURL, urlStr)

	buf := new(bytes.Buffer)
	if body != nil {
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, u, buf)
	if err != nil {
		return nil, err
	}

	if c.token != "" {
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.token))
	}

	if body != nil {
		req.Header.Add("Content-Type", "application/json")
	}

	return req, nil
}

func (c *Client) doRequest(req *http.Request, v interface{}) (*http.Response, error) {
	var (
		resp *http.Response
		err  error
	)

	resp, err = c.httpCli.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	defer io.Copy(ioutil.Discard, resp.Body)

	err = checkResponse(resp)
	if err != nil {
		return resp, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			io.Copy(w, resp.Body)
		} else {
			err = json.NewDecoder(resp.Body).Decode(v)
		}
	}

	return resp, err
}

func checkResponse(r *http.Response) error {
	if c := r.StatusCode; 200 <= c && c <= 299 {
		return nil
	}

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	yandexError := &Error{}
	errDec := json.Unmarshal(data, yandexError)
	if errDec == nil {
		return yandexError
	}

	return fmt.Errorf("failed: %s", string(data))
}

func joinURLPath(endpoint string, path string) string {
	return strings.TrimRight(endpoint, "/") + "/" + strings.TrimLeft(path, "/")
}
