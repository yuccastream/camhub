package yandex

// DeviceInfoResponse
// https://yandex.ru/dev/dialogs/smart-home/doc/concepts/platform-device-info.html
//
// {
//    "status": "ok",
//    "request_id": "d887f89b-431d-43ea-88c5-ab736afcd394",
//    "id": "cbf997bb-0de8-4de0-8df7-39b828be800d",
//    "name": "Розетка",
//    "aliases": [],
//    "type": "devices.types.socket",
//    "external_id": "bf3d656c1b474c6200bnke",
//    "skill_id": "T",
//    "state": "online",
//    "groups": [],
//    "room": "fe915b32-ac43-416c-8f1b-4793afedc0d0",
//    "capabilities": [{
//        "retrievable": true,
//        "type": "devices.capabilities.on_off",
//        "parameters": {
//            "split": false
//        },
//        "state": {
//            "instance": "on",
//            "value": true
//        },
//        "last_updated": 1645447969.4486325
//    }],
//    "properties": []
// }
type DeviceInfoResponse struct {
	Status       string              `json:"status"`
	RequestID    string              `json:"request_id"`
	ID           string              `json:"id"`
	Name         string              `json:"name"`
	Aliases      []string            `json:"aliases"`
	Type         string              `json:"type"`
	ExternalID   string              `json:"external_id"`
	SkillID      string              `json:"skill_id"`
	State        string              `json:"state"`
	Groups       []string            `json:"groups"`
	Capabilities []*CapabilityObject `json:"capabilities"`
}

// DeviceActionsResponse
// https://yandex.ru/dev/dialogs/smart-home/doc/concepts/platform-capabilities.html
//
// {
//    "status": "ok",
//    "request_id": "83959396-8750-467d-abdb-88170e74bdab",
//    "devices": [{
//        "id": "cbf997bb-0de8-4de0-8df7-39b828be800d",
//        "capabilities": [{
//            "type": "devices.capabilities.on_off",
//            "state": {
//                "instance": "on",
//                "action_result": {
//                    "status": "DONE"
//                }
//            }
//        }]
//    }]
// }
type DeviceActionsResponse struct {
	RequestID string                      `json:"request_id"`
	Status    string                      `json:"status"`
	Devices   []*DeviceActionResultObject `json:"devices"`
}

type DeviceActionResultObject struct {
	ID           string                          `json:"id"`
	Capabilities []*CapabilityActionResultObject `json:"capabilities"`
}

type CapabilityActionResultObject struct {
	Type  string            `json:"type"`
	State StateResultObject `json:"state"`
}

type StateResultObject struct {
	Instance     string             `json:"instance"`
	ActionResult ActionResultObject `json:"action_result"`
}

type ActionResultObject struct {
	Status       string `json:"status"`
	ErrorCode    string `json:"error_code"`
	ErrorMessage string `json:"error_message"`
}

// DeviceActionsRequest
// https://yandex.ru/dev/dialogs/smart-home/doc/concepts/platform-capabilities.html
//
// {
//    "devices": [{
//        "id": "cbf997bb-0de8-4de0-8df7-39b828be800d",
//        "actions": [{
//            "type": "devices.capabilities.on_off",
//            "state": {
//                "instance": "on",
//                "value": false
//            }
//        }]
//    }]
// }
type DeviceActionsRequest struct {
	Devices []*DeviceActionObject `json:"devices"`
}

type DeviceActionObject struct {
	ID      string              `json:"id"`
	Actions []*CapabilityObject `json:"actions"`
}

type CapabilityObject struct {
	Type  string                `json:"type"`
	State CapabilityStateObject `json:"state"`
}

type CapabilityStateObject struct {
	Instance string `json:"instance"`
	Value    bool   `json:"value"`
}
