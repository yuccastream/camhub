package yandex

import "fmt"

type Error struct {
	RequestID string `json:"request_id"`
	Status    string `json:"status"`
	Message   string `json:"message"`
}

func (e Error) Error() string {
	return fmt.Sprintf("Error [status=%s, message=%s]", e.Status, e.Message)
}
