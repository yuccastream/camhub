package yandex

import (
	"fmt"
	"net/http"
)

func (c *Client) DeviceInfo(id string) (*DeviceInfoResponse, *http.Response, error) {
	req, err := c.newRequest(http.MethodGet, fmt.Sprintf("devices/%s", id), nil)
	if err != nil {
		return nil, nil, err
	}
	deviceInfo := &DeviceInfoResponse{}
	resp, err := c.doRequest(req, deviceInfo)
	return deviceInfo, resp, err
}

func (c *Client) DeviceCapabilitiesOnOff(id string, value bool) (*DeviceActionsResponse, *http.Response, error) {
	capability := &CapabilityObject{
		Type: "devices.capabilities.on_off",
		State: CapabilityStateObject{
			Instance: "on",
			Value:    value,
		},
	}
	device := &DeviceActionObject{
		ID:      id,
		Actions: []*CapabilityObject{capability},
	}
	body := &DeviceActionsRequest{
		Devices: []*DeviceActionObject{device},
	}
	req, err := c.newRequest(http.MethodPost, "devices/actions", body)
	if err != nil {
		return nil, nil, err
	}
	deviceActions := &DeviceActionsResponse{}
	resp, err := c.doRequest(req, deviceActions)
	return deviceActions, resp, err
}
