package main

import (
	"encoding/json"
	"net/http"
)

type ErrorResponse struct {
	Error, DetailedError string
}

func WriteBadRequest(w http.ResponseWriter, reason string) {
	w.WriteHeader(http.StatusBadRequest)
	resp := ErrorResponse{Error: reason}
	err := json.NewEncoder(w).Encode(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func WriteInternalServerError(w http.ResponseWriter, friendlyMessage, detailedMessage string) {
	w.WriteHeader(http.StatusInternalServerError)
	resp := ErrorResponse{Error: friendlyMessage, DetailedError: detailedMessage}
	err := json.NewEncoder(w).Encode(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
