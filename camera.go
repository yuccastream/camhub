package main

import (
	"bytes"
	"net/url"
	"text/template"
	"time"
)

type Camera struct {
	Name      string            `yaml:"name" json:"name"`
	MatchID   string            `yaml:"matchID" json:"-"`
	MatchName string            `yaml:"matchName" json:"-"`
	Login     string            `yaml:"login" json:"login"`
	Password  string            `yaml:"password" json:"password"`
	LinksRaw  map[string]string `yaml:"links" json:"-"`

	ID        int               `yaml:"-" json:"id"`
	IP        string            `yaml:"-" json:"ip"`
	Online    bool              `yaml:"-" json:"online"`
	UpdateAt  time.Time         `yaml:"-" json:"updateAt"`
	XAddr     string            `yaml:"-" json:"xaddr"`
	XAddrBase string            `yaml:"-" json:"xaddrBase"`
	Links     map[string]string `yaml:"-" json:"links"`
}

func (c *Camera) offline() {
	c.Online = false
	c.UpdateAt = time.Now()
	c.XAddr = ""
	c.IP = ""
	c.Links = nil
}

func (c *Camera) SetXAddr(xaddr string) {
	c.XAddr = xaddr
	u, err := url.Parse(xaddr)
	if err == nil {
		c.IP = u.Hostname()
		port := u.Port()
		if port == "" {
			port = "80"
		}
		c.XAddrBase = u.Hostname() + ":" + port
	}
}

func (c *Camera) ProcessLinks() {
	for name, link := range c.LinksRaw {
		buf := bytes.NewBufferString("")
		tmpl, err := template.New(name).Parse(link)
		if err != nil {
			continue
		}
		err = tmpl.Execute(buf, c)
		if err != nil {
			continue
		}
		c.Links[name] = buf.String()
	}
}

func (c *Camera) online(xaddr string) {
	c.Online = true
	c.UpdateAt = time.Now()
	c.Links = make(map[string]string)
	c.SetXAddr(xaddr)
	c.ProcessLinks()
}
