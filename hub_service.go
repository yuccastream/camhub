package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func (h *Hub) GetHubStatus(w http.ResponseWriter, r *http.Request) {
	status, err := h.CurrentStatus()
	if err != nil {
		WriteInternalServerError(w, "Failed to get status", fmt.Sprintf("Failed to get status: %v", err))
		return
	}
	if err := json.NewEncoder(w).Encode(status); err != nil {
		WriteInternalServerError(w, "Failed to get status", fmt.Sprintf("Failed to get status: %v", err))
		return
	}
}

func (h *Hub) SetHubOn(w http.ResponseWriter, r *http.Request) {
	resp, err := h.SwitchDevice(true)
	if err != nil {
		WriteInternalServerError(w, "Failed to switch status", fmt.Sprintf("Failed to switch status: %v", err))
		return
	}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		WriteInternalServerError(w, "Failed to switch status", fmt.Sprintf("Failed to switch status: %v", err))
		return
	}
}

func (h *Hub) SetHubOff(w http.ResponseWriter, r *http.Request) {
	resp, err := h.SwitchDevice(false)
	if err != nil {
		WriteInternalServerError(w, "Failed to switch status", fmt.Sprintf("Failed to switch status: %v", err))
		return
	}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		WriteInternalServerError(w, "Failed to switch status", fmt.Sprintf("Failed to switch status: %v", err))
		return
	}
}
