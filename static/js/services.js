angular.module('myApp.services', [])

    .service('API', function ($http) {
        this.camera = function (id) {
            return $http.get('/v1/cameras/' + id);
        }
        this.cameras = function () {
            return $http.get('/v1/cameras');
        }
        this.status = function () {
            return $http.get('/v1/hub/status');
        }
        this.on = function () {
            return $http.post('/v1/hub/on');
        }
        this.off = function () {
            return $http.post('/v1/hub/off');
        }
    });
