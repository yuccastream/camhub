angular.module('myApp', [
    'myApp.controllers',
    'myApp.services',
    'ngRoute',
    'ngMaterial',
])

    .config(function($httpProvider, $routeProvider, $locationProvider, $compileProvider) {
        $httpProvider.useApplyAsync(true);
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|rtsp|mailto|chrome-extension):/);

        $routeProvider
            .when('/home', {
                templateUrl: 'partials/views/home.html',
                controller: 'HomeController',
            })

            .when('/camera/:id', {
                templateUrl: 'partials/views/camera.html',
                controller: 'CameraController',
            })

            .otherwise('/home');

        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix('!');
    });
