angular.module('myApp.controllers', [])

    .controller('CameraController', function ($scope, $routeParams, $interval, API) {
        $scope.id = $routeParams.id;
        $scope.preview = new Date();
        $scope.status = {};
        $scope.loaded = false;
        $scope.camera = {};
        var stop;

        $scope.start = function() {
            API.status()
                .then(function (result) {
                    $scope.status = result.data;
                    $scope.loaded = true;
                });
            API.camera($scope.id)
                .then(function (result) {
                    $scope.camera = result.data;

                    if ( angular.isDefined(stop) ) return;
                    stop = $interval(function() {
                        $scope.preview = new Date();
                    }, 2000);
                });
        };

        $scope.$on('$destroy', function() {
            if (angular.isDefined(stop)) {
                $interval.cancel(stop);
                stop = undefined;
            }
        });

        $scope.start();
    })

    .controller('HomeController', function ($scope, $interval, API) {
        $scope.loaded = false;
        $scope.status = {};
        $scope.cameras = [];
        var stop;

        $scope.refresh = function () {
            API.cameras()
                .then(function (result) {
                    $scope.cameras = result.data;
                });
        };

        $scope.start = function() {
            API.status()
                .then(function (result) {
                    $scope.status = result.data;
                    $scope.loaded = true;
                });
            API.cameras()
                .then(function (result) {
                    $scope.cameras = result.data;

                    if ( angular.isDefined(stop) ) return;
                    stop = $interval(function() {
                        $scope.refresh();
                    }, 1000);
                });
        };

        $scope.onChange = function (value) {
            if (value === false) {
                API.off().then(function () {
                    $scope.status.stand.on = false;
                });
            } else {
                API.on().then(function () {
                    $scope.status.stand.on = true;
                });
            }
        };

        $scope.showCamera = function(entry) {
            window.location = '#!/camera/'+entry.id;
        };

        $scope.$on('$destroy', function() {
            if (angular.isDefined(stop)) {
                $interval.cancel(stop);
                stop = undefined;
            }
        });

        $scope.start();
    });
