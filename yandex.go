package main

import (
	"net/http"

	"github.com/sirupsen/logrus"

	"gitlab.com/yuccastream/camhub/yandex"
)

type YandexClient interface {
	DeviceInfo(id string) (*yandex.DeviceInfoResponse, *http.Response, error)
	DeviceCapabilitiesOnOff(id string, value bool) (*yandex.DeviceActionsResponse, *http.Response, error)
}

type NoOpYandexClient struct{}

func NewYandexClient(token string) YandexClient {
	if token == "" {
		logrus.Warn("Yandex Token not found, no-op client will be used")
		return new(NoOpYandexClient)
	}
	return yandex.NewClient(token)
}

func (n *NoOpYandexClient) DeviceInfo(_ string) (*yandex.DeviceInfoResponse, *http.Response, error) {
	d := &yandex.DeviceInfoResponse{
		Capabilities: []*yandex.CapabilityObject{
			{
				State: yandex.CapabilityStateObject{
					Value: true,
				},
			},
		},
	}
	return d, nil, nil
}

func (n *NoOpYandexClient) DeviceCapabilitiesOnOff(_ string, _ bool) (*yandex.DeviceActionsResponse, *http.Response, error) {
	return &yandex.DeviceActionsResponse{}, nil, nil
}
