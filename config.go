package main

import (
	"os"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Cameras       []*Camera `yaml:"cameras"`
	ListenAddress string    `envconfig:"LISTEN_ADDRESS" yaml:"listenAddress" default:"0.0.0.0:8080"`
	YandexIoT     struct {
		Token    string `envconfig:"YANDEX_IOT_TOKEN" yaml:"token"`
		DeviceID string `envconfig:"YANDEX_IOT_DEVICE_ID" yaml:"deviceID"`
	} `yaml:"yandexIoT"`
}

func LoadFromEnv() *Config {
	c := &Config{}
	envconfig.MustProcess("", c)
	return c
}

func (c *Config) EnrichFromFile(filename string) error {
	b, err := os.ReadFile(filename)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(b, c)
	if err != nil {
		return err
	}
	for i, cam := range c.Cameras {
		cam.ID = i + 1
	}
	return nil
}
