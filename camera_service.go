package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func (h *Hub) processCamerasForOnline(devices []Device) {
	for _, d := range devices {
		for _, c := range h.config.Cameras {
			if (c.MatchName == d.Name || c.MatchID == d.ID) && !c.Online {
				logrus.WithField("name", c.Name).Info("Switched to online")
				c.online(d.XAddr)
			}
		}
	}
}

func (h *Hub) processCamerasForOffline() {
	for _, c := range h.config.Cameras {
		if !c.Online && !c.UpdateAt.IsZero() && time.Since(c.UpdateAt) > time.Minute {
			logrus.WithField("name", c.Name).Info("Switched to offline")
			c.offline()
		}
	}
}

func (h *Hub) discoveryCameras() {
	if on, err := h.DeviceInfo(); err == nil && !on {
		h.offlineCameras()
		return
	}

	logrus.Info("Discovering cameras...")

	devices, err := StartDiscovery("", 10*time.Second)
	if err != nil {
		logrus.WithError(err).Error("Failed to discovery cameras")
		return
	}

	h.processCamerasForOnline(devices)
	h.processCamerasForOffline()
}

func (h *Hub) discoveryCamerasRoutine() {
	ticker := time.NewTicker(20 * time.Second).C
	h.discoveryCameras()
	for {
		select {
		case <-ticker:
			h.discoveryCameras()
		}
	}
}

func (h *Hub) offlineCameras() {
	for _, cam := range h.config.Cameras {
		if cam.Online {
			cam.offline()
		}
	}
}

func (h *Hub) GetCameraByID(id int) *Camera {
	for _, cam := range h.config.Cameras {
		if cam.ID == id {
			return cam
		}
	}
	return nil
}

func (h *Hub) GetCameras(w http.ResponseWriter, r *http.Request) {
	if err := json.NewEncoder(w).Encode(h.config.Cameras); err != nil {
		WriteInternalServerError(w, "Failed to get cameras", fmt.Sprintf("Failed to get cameras: %v", err))
		return
	}
}

func (h *Hub) GetCamera(w http.ResponseWriter, r *http.Request) {
	cameraIDRaw, ok := mux.Vars(r)["id"]
	if !ok {
		WriteBadRequest(w, "Camera ID not specified")
		return
	}
	cameraID, err := strconv.Atoi(cameraIDRaw)
	if err != nil {
		WriteBadRequest(w, "Incorrect camera ID")
		return
	}
	cam := h.GetCameraByID(cameraID)
	if cam == nil {
		WriteBadRequest(w, "Camera not found")
		return
	}
	if err := json.NewEncoder(w).Encode(cam); err != nil {
		WriteInternalServerError(w, "Failed to get camera", fmt.Sprintf("Failed to get camera: %v", err))
		return
	}
}
