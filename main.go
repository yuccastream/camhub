package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
)

var (
	configFile = flag.String("config", "config.yaml", "Path to configuration file")
)

func main() {
	flag.Parse()

	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.Info("Starting camhub...")

	config := LoadFromEnv()
	if *configFile != "" {
		if _, err := os.Stat(*configFile); err == nil {
			err := config.EnrichFromFile(*configFile)
			if err != nil {
				logrus.Fatal(err)
			}
		} else {
			logrus.Warn(err)
		}
	}

	logrus.Infof("Device ID: %s", config.YandexIoT.DeviceID)
	logrus.Infof("Cameras: %d", len(config.Cameras))
	hub := NewHub(config)

	go func() {
		logrus.Infof("Listen address: %s", config.ListenAddress)
		err := hub.Serve()
		logrus.WithError(err).Fatal("Exited")
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	<-stop

	logrus.Info("Shutting down")
	if err := hub.Stop(); err != nil {
		logrus.Warn(err)
	}
	logrus.Info("Bye")
}
