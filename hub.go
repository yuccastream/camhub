package main

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/yuccastream/camhub/yandex"
)

type Hub struct {
	router http.Handler
	cli    YandexClient
	config *Config
}

func NewHub(config *Config) *Hub {
	cli := NewYandexClient(config.YandexIoT.Token)
	router := mux.NewRouter()
	router.Use(AccessLog)
	h := &Hub{
		cli:    cli,
		router: router,
		config: config,
	}
	h.setV1Routes(router)
	h.setStaticRoute(router)
	return h
}

func (h *Hub) Serve() error {
	go h.discoveryCamerasRoutine()
	return http.ListenAndServe(h.config.ListenAddress, h.router)
}

func (h *Hub) Stop() error {
	return nil
}

type Status struct {
	Stand struct {
		On bool `json:"on"`
	} `json:"stand"`
}

func (h *Hub) DeviceInfo() (bool, error) {
	info, _, err := h.cli.DeviceInfo(h.config.YandexIoT.DeviceID)
	if err != nil {
		return false, err
	}
	if len(info.Capabilities) == 0 {
		return false, nil
	}
	capability := info.Capabilities[0]
	return capability.State.Value, nil
}

func (h *Hub) CurrentStatus() (*Status, error) {
	status := &Status{}
	on, err := h.DeviceInfo()
	if err != nil {
		return nil, err
	}
	status.Stand.On = on
	return status, nil
}

func (h *Hub) SwitchDevice(value bool) (*yandex.DeviceActionsResponse, error) {
	resp, _, err := h.cli.DeviceCapabilitiesOnOff(h.config.YandexIoT.DeviceID, value)
	return resp, err
}
