package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/yuccastream/camhub/yandex"
)

var (
	token    = flag.String("t", os.Getenv("YANDEX_IOT_TOKEN"), "Token")
	deviceID = flag.String("id", os.Getenv("YANDEX_IOT_DEVICE_ID"), "Device ID")
)

func main() {
	flag.Parse()

	cli := yandex.NewClient(*token)
	info, _, err := cli.DeviceInfo(*deviceID)
	if err != nil {
		log.Fatal(err)
	}

	capability := info.Capabilities[0]
	_, _, err = cli.DeviceCapabilitiesOnOff(*deviceID, !capability.State.Value)
	if err != nil {
		log.Fatal(err)
	}
}
