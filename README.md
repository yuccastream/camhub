# camhub

Сервер для организации тестового стенда для работы с камерами. Включение и отключение стенда реализовано через Умную Розетку от Yandex, поиск камер – ONVIF.

![screenshot](screenshot.png)

## Конфигурация

Список камер используется для описания реквизитов и специфических ссылок, которые будут использоваться в Web-интерфейсе. Связь обнаруженной по ONVIF камеры с камерой из списка осуществляется по полю `matchName` или `matchID`. Без передачи Yandex Token Умная Розетка использоваться не будет.

```yaml
---
listenAddress: 0.0.0.0:8080 # env: LISTEN_ADDRESS
yandexIoT:
  token: ABCD # env: YANDEX_IOT_TOKEN
  deviceID: ABCD # env: YANDEX_IOT_DEVICE_ID
cameras:
  - name: HiWatch
    matchName: DS-I214W
    # matchID: 812b12f7-45a0-11b5-8404-0c8c24a530f4
    login: admin
    password: admin
    links:
      Main: rtsp://{{.Login}}:{{.Password}}@{{.IP}}:554/Streaming/Channels/101
      Sub: rtsp://{{.Login}}:{{.Password}}@{{.IP}}:554/mjpeg/ch1/sub/av_stream
      Preview: http://{{.Login}}:{{.Password}}@{{.IP}}/ISAPI/Streaming/channels/101/picture?snapShotImageType=JPEG
```

Путь к конфигурационному файлу может быть переопределен через флаг `--config`.

## Установка

```shell
GOOS=linux GOARCH=arm go build .
scp ./camhub root@raspberrypi.local:/opt/camhub/
scp ./config.yaml root@raspberrypi.local:/opt/camhub/
scp ./camhub.service root@raspberrypi.local:/etc/systemd/system/
```
