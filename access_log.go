package main

import (
	"bytes"
	"net/http"
	"strings"

	"github.com/gorilla/handlers"
	"github.com/sirupsen/logrus"
)

type statusRecorder struct {
	http.ResponseWriter
	status int
}

func (r *statusRecorder) WriteHeader(code int) {
	r.status = code
	r.ResponseWriter.WriteHeader(code)
}

func AccessLog(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		buf := bytes.NewBufferString("")
		rec := &statusRecorder{w, http.StatusOK}
		handlers.CombinedLoggingHandler(buf, h).ServeHTTP(rec, r)
		if rec.status >= http.StatusInternalServerError {
			logrus.Error(strings.TrimSuffix(buf.String(), "\n"))
		} else {
			logrus.Info(strings.TrimSuffix(buf.String(), "\n"))
		}
	})
}
