package main

import (
	"embed"
	"io/fs"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

var (
	//go:embed static/*
	staticFS embed.FS
)

func (h *Hub) setStaticRoute(router *mux.Router) {
	fsys, err := fs.Sub(staticFS, "static")
	if err != nil {
		logrus.Fatal(err)
	}
	router.PathPrefix("/").Handler(http.FileServer(http.FS(fsys)))
}
